module gitlab.com/go-reusables/config

go 1.12

require (
	github.com/buger/jsonparser v0.0.0-20181115193947-bf1c66bbce23
	github.com/pkg/errors v0.8.1
)
