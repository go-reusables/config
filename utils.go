package config

// Setting is a simple alias type to enable code preprocessing
// tools to gather information about supported configuration
// parameters. Use `Setting` wherever you need to pass a config
// parameter name.
type Setting = string
