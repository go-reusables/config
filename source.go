package config

import (
	"strconv"

	"github.com/pkg/errors"
)

// Source represents a source of values. This allows us
// to facilitate a mechanism where multiple configuration
// environments can be used.
type Source interface {
	Resolve(name string) (bool, string)
}

func errConversion(err error, name string) error {
	return errors.Wrapf(err, "failed converting setting %s", name)
}

// ResolveInt resolves an integer setting from a given source.
// Panics in case the conversion of the value fails with an
// appropriate message.
func ResolveInt(source Source, name string) (bool, int) {
	found, value := source.Resolve(name)

	if !found {
		return false, 0
	}

	valueInt, err := strconv.Atoi(value)
	if err != nil {
		panic(errConversion(err, name))
	}

	return true, valueInt
}

// ResolveStr resolves an string setting from a given source.
// Panics in case the conversion of the value fails with an
// appropriate message.
func ResolveStr(source Source, name string) (bool, string) {
	found, value := source.Resolve(name)

	if !found {
		return false, ""
	}

	return true, value
}

// ResolveBool resolves a boolean setting from a given source.
// Panics in case the conversion of the value fails with an
// appropriate message.
func ResolveBool(source Source, name string) (bool, bool) {
	found, value := source.Resolve(name)

	if !found {
		return false, false
	}

	valueBool, err := strconv.ParseBool(value)
	if err != nil {
		panic(errConversion(err, name))
	}

	return true, valueBool
}
