package sources

import (
	"io/ioutil"
	
	"github.com/buger/jsonparser"
)

// JSONSource represents a JSON file which is supposed to
// contain the values of the required settings.
type JSONSource struct {
	Data []byte
}

// Resolve is contractual function to resolve the value of a
// setting from its setting name.
func (source JSONSource) Resolve(name string) (bool, string) {
	value, _, _, err := jsonparser.Get(source.Data, name)

	if err != nil {
		return false, ""
	}

	return true, string(value)
}

// NewJSONSource returns a new JSONSource structure from raw
// JSON data passed as a string.
func NewJSONSource(source string) JSONSource {
	return JSONSource{
		Data: []byte(source),
	}
}

// NewJSONSourceFromFile returns a new JSONSource initialized
// from data from a file. Filename needs to be passed.
func NewJSONSourceFromFile(file string) JSONSource {
	data, err := ioutil.ReadFile(file)

	if err != nil {
		panic(err)
	}

	return JSONSource{
		Data: data,
	}
}
