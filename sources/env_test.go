package sources

import (
	"os"
	"testing"

	"gitlab.com/go-reusables/config"
)

func must(err error) {
	if err != nil {
		panic("failed setting environment variable")
	}
}

func envSetup() {
	must(os.Setenv("TEST_SAMPLE_INT", "3"))
	must(os.Setenv("TEST_SAMPLE_STR", "HELLO"))
	must(os.Setenv("TEST_SAMPLE_BOOL", "true"))
}

func TestEnvSource(t *testing.T) {
	envSetup()
	envSource := NewEnvSource()

	if _, value := config.ResolveInt(envSource, "TEST_SAMPLE_INT"); value != 3 {
		t.Error("Test failed for ResolveInt")
	}

	if _, value := config.ResolveStr(envSource, "TEST_SAMPLE_STR"); value != "HELLO" {
		t.Error("Test failed for ResolveStr")
	}

	if _, value := config.ResolveBool(envSource, "TEST_SAMPLE_BOOL"); value != true {
		t.Error("Test failed for ResolveBool")
	}
}
