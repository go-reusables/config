package sources

import (
	"os"
)

// EnvSource fetches the required settings from environment.
// Setting environment variables will populate settings.
type EnvSource struct {
}

// Resolve resolves the name of a setting to its string value.
// Parsing / Conversion of values is taken care of by other available
// utility functions in the library.
func (source EnvSource) Resolve(name string) (bool, string) {
	value, found := os.LookupEnv(name)
	return found, value
}

// NewEnvSource returns a new EnvSource
func NewEnvSource() EnvSource {
	return EnvSource{}
}
