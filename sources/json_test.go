package sources

import (
	"testing"

	"gitlab.com/go-reusables/config"
)

const (
	json = `
		{
			"TEST_SAMPLE_INT": 3,
			"TEST_SAMPLE_BOOL": true,
			"TEST_SAMPLE_STR": "HELLO"
		}
	`
)

func TestJSONSource(t *testing.T) {
	jsonSource := NewJSONSource(json)

	if _, value := config.ResolveInt(jsonSource, "TEST_SAMPLE_INT"); value != 3 {
		t.Error("Test failed for ResolveInt")
	}

	if _, value := config.ResolveStr(jsonSource, "TEST_SAMPLE_STR"); value != "HELLO" {
		t.Error("Test failed for ResolveStr")
	}

	if _, value := config.ResolveBool(jsonSource, "TEST_SAMPLE_BOOL"); value != true {
		t.Error("Test failed for ResolveBool")
	}
}
